# GBCR register map classes
Supported versions:
- GBCR2

All values taken from the GBCR2 manual v2.1 [EDMS](https://edms.cern.ch/ui/#!master/navigator/document?P:100324489:100646067:subDocs)

`GBCR2_Reg.py` script made by Wei Zhang.

Inspired by the [lpgbt_control_lib](https://gitlab.cern.ch/lpgbt/lpgbt_control_lib) register maps.

Feel free to use/fork, cite Bern ATLAS group :)