#!/usr/bin/env python3

# GBCR2 register map inspired by lpgbt_control_lib
# author: Roman Mueller, Bern


"""GBCR2 Constants"""

from enum import IntEnum, unique


class GBCRRegisterMap:
    """Class containing all GBCR-related constants"""

    class CH1UPLINK0:
        """Uplink channel 1 settings (1/3)"""
        address = 0x00

        @staticmethod
        def __str__():
            return "CH1UPLINK0"

        @staticmethod
        def __int__():
            return 0x00

        class CH1DISEQLF:
            """disable the low Frequency CTLE Stage"""

            offset = 5
            length = 1
            bit_mask = 32
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)

        class CH1EQATT:
            """Set attenuator in EQ"""

            offset = 3
            length = 2
            bit_mask = 24
            default = 3

            @staticmethod
            def validate(value):
                return value in range(4)

        class CH1CMLAMPLSEL:
            """Set the output amplitude"""

            offset = 0
            length = 3
            bit_mask = 7
            default = 7

            @staticmethod
            def validate(value):
                return value in range(8)

    class CH1UPLINK1:
        """Uplink channel 1 settings (2/3)"""

        address = 0x01

        @staticmethod
        def __str__():
            return "CH1UPLINK1"

        @staticmethod
        def __int__():
            return 0x01

        class CH1CTLEHFSR:
            """Set the high Frequency CTLE peaking strength"""

            offset = 4
            length = 4
            bit_mask = 240
            default = 11

            @staticmethod
            def validate(value):
                return value in range(16)

        class CH1CTLEMFSR:
            """Set the middle Frequency CTLE peaking strength"""

            offset = 0
            length = 4
            bit_mask = 15
            default = 11

            @staticmethod
            def validate(value):
                return value in range(16)

    class CH1UPLINK2:
        """Uplink channel 1 settings (3/3)"""

        address = 0x02

        @staticmethod
        def __str__():
            return "CH1UPLINK2"

        @staticmethod
        def __int__():
            return 0x02

        class CH1DIS:
            """Disable channel"""

            offset = 2
            length = 1
            bit_mask = 4
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)

        class CH1DISDFF:
            """Set Equalizer mode"""

            offset = 1
            length = 1
            bit_mask = 2
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)

        class CH1DISLPF:
            """disable DC offset cancellation"""

            offset = 0
            length = 1
            bit_mask = 1
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)

    class CH2UPLINK0:

        address = 0x03

        @staticmethod
        def __str__():
            return "CH2UPLINK0"

        @staticmethod
        def __int__():
            return 0x03

        class CH2DISEQLF:
            """disable the low Frequency CTLE Stage"""

            offset = 5
            length = 1
            bit_mask = 32
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)

        class CH2EQATT:
            """Set attenuator in EQ"""

            offset = 3
            length = 2
            bit_mask = 24
            default = 3

            @staticmethod
            def validate(value):
                return value in range(4)

        class CH2CMLAMPLSEL:
            """Set the output amplitude"""

            offset = 0
            length = 3
            bit_mask = 7
            default = 7

            @staticmethod
            def validate(value):
                return value in range(8)

    class CH2UPLINK1:

        address = 0x04

        @staticmethod
        def __str__():
            return "CH2UPLINK1"

        @staticmethod
        def __int__():
            return 0x04

        class CH2CTLEHFSR:
            """Set the high Frequency CTLE peaking strength"""

            offset = 4
            length = 4
            bit_mask = 240
            default = 11

            @staticmethod
            def validate(value):
                return value in range(16)

        class CH2CTLEMFSR:
            """Set the middle Frequency CTLE peaking strength"""

            offset = 0
            length = 4
            bit_mask = 15
            default = 11

            @staticmethod
            def validate(value):
                return value in range(16)

    class CH2UPLINK2:

        address = 0x05

        @staticmethod
        def __str__():
            return "CH2UPLINK2"

        @staticmethod
        def __int__():
            return 0x05

        class CH2DIS:
            """Disable channel"""

            offset = 2
            length = 1
            bit_mask = 4
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)

        class CH2DISDFF:
            """Set Equalizer mode"""

            offset = 1
            length = 1
            bit_mask = 2
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)

        class CH2DISLPF:
            """disable DC offset cancellation"""

            offset = 0
            length = 1
            bit_mask = 1
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)

    class CH3UPLINK0:

        address = 0x06

        @staticmethod
        def __str__():
            return "CH3UPLINK0"

        @staticmethod
        def __int__():
            return 0x06

        class CH3DISEQLF:
            """disable the low Frequency CTLE Stage"""

            offset = 5
            length = 1
            bit_mask = 32
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)

        class CH3EQATT:
            """Set attenuator in EQ"""

            offset = 3
            length = 2
            bit_mask = 24
            default = 3

            @staticmethod
            def validate(value):
                return value in range(4)

        class CH3CMLAMPLSEL:
            """Set the output amplitude"""

            offset = 0
            length = 3
            bit_mask = 7
            default = 7

            @staticmethod
            def validate(value):
                return value in range(8)

    class CH3UPLINK1:

        address = 0x07

        @staticmethod
        def __str__():
            return "CH3UPLINK1"

        @staticmethod
        def __int__():
            return 0x07

        class CH3CTLEHFSR:
            """Set the high Frequency CTLE peaking strength"""

            offset = 4
            length = 4
            bit_mask = 240
            default = 11

            @staticmethod
            def validate(value):
                return value in range(16)

        class CH3CTLEMFSR:
            """Set the middle Frequency CTLE peaking strength"""

            offset = 0
            length = 4
            bit_mask = 15
            default = 11

            @staticmethod
            def validate(value):
                return value in range(16)

    class CH3UPLINK2:

        address = 0x08

        @staticmethod
        def __str__():
            return "CH3UPLINK2"

        @staticmethod
        def __int__():
            return 0x08

        class CH3DIS:
            """Disable channel"""

            offset = 2
            length = 1
            bit_mask = 4
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)

        class CH3DISDFF:
            """Set Equalizer mode"""

            offset = 1
            length = 1
            bit_mask = 2
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)

        class CH3DISLPF:
            """disable DC offset cancellation"""

            offset = 0
            length = 1
            bit_mask = 1
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)

    class CH4UPLINK0:

        address = 0x09

        @staticmethod
        def __str__():
            return "CH4UPLINK0"

        @staticmethod
        def __int__():
            return 0x09

        class CH4DISEQLF:
            """disable the low Frequency CTLE Stage"""

            offset = 5
            length = 1
            bit_mask = 32
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)

        class CH4EQATT:
            """Set attenuator in EQ"""

            offset = 3
            length = 2
            bit_mask = 24
            default = 3

            @staticmethod
            def validate(value):
                return value in range(4)

        class CH4CMLAMPLSEL:
            """Set the output amplitude"""

            offset = 0
            length = 3
            bit_mask = 7
            default = 7

            @staticmethod
            def validate(value):
                return value in range(8)

    class CH4UPLINK1:

        address = 0x0a

        @staticmethod
        def __str__():
            return "CH4UPLINK1"

        @staticmethod
        def __int__():
            return 0x0a

        class CH4CTLEHFSR:
            """Set the high Frequency CTLE peaking strength"""

            offset = 4
            length = 4
            bit_mask = 240
            default = 11

            @staticmethod
            def validate(value):
                return value in range(16)

        class CH4CTLEMFSR:
            """Set the middle Frequency CTLE peaking strength"""

            offset = 0
            length = 4
            bit_mask = 15
            default = 11

            @staticmethod
            def validate(value):
                return value in range(16)

    class CH4UPLINK2:

        address = 0x0b

        @staticmethod
        def __str__():
            return "CH4UPLINK2"

        @staticmethod
        def __int__():
            return 0x0b

        class CH4DIS:
            """Disable channel"""

            offset = 2
            length = 1
            bit_mask = 4
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)

        class CH4DISDFF:
            """Set Equalizer mode"""

            offset = 1
            length = 1
            bit_mask = 2
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)

        class CH4DISLPF:
            """disable DC offset cancellation"""

            offset = 0
            length = 1
            bit_mask = 1
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)

    class CH5UPLINK0:

        address = 0x0c

        @staticmethod
        def __str__():
            return "CH5UPLINK0"

        @staticmethod
        def __int__():
            return 0x0c

        class CH5DISEQLF:
            """disable the low Frequency CTLE Stage"""

            offset = 5
            length = 1
            bit_mask = 32
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)

        class CH5EQATT:
            """Set attenuator in EQ"""

            offset = 3
            length = 2
            bit_mask = 24
            default = 3

            @staticmethod
            def validate(value):
                return value in range(4)

        class CH5CMLAMPLSEL:
            """Set the output amplitude"""

            offset = 0
            length = 3
            bit_mask = 7
            default = 7

            @staticmethod
            def validate(value):
                return value in range(8)

    class CH5UPLINK1:

        address = 0x0d

        @staticmethod
        def __str__():
            return "CH5UPLINK1"

        @staticmethod
        def __int__():
            return 0x0d

        class CH5CTLEHFSR:
            """Set the high Frequency CTLE peaking strength"""

            offset = 4
            length = 4
            bit_mask = 240
            default = 11

            @staticmethod
            def validate(value):
                return value in range(16)

        class CH5CTLEMFSR:
            """Set the middle Frequency CTLE peaking strength"""

            offset = 0
            length = 4
            bit_mask = 15
            default = 11

            @staticmethod
            def validate(value):
                return value in range(16)

    class CH5UPLINK2:

        address = 0x0e

        @staticmethod
        def __str__():
            return "CH5UPLINK2"

        @staticmethod
        def __int__():
            return 0x0e

        class CH5DIS:
            """Disable channel"""

            offset = 2
            length = 1
            bit_mask = 4
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)

        class CH5DISDFF:
            """Set Equalizer mode"""

            offset = 1
            length = 1
            bit_mask = 2
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)

        class CH5DISLPF:
            """disable DC offset cancellation"""

            offset = 0
            length = 1
            bit_mask = 1
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)

    class CH6UPLINK0:

        address = 0x0f

        @staticmethod
        def __str__():
            return "CH6UPLINK0"

        @staticmethod
        def __int__():
            return 0x0f

        class CH6DISEQLF:
            """disable the low Frequency CTLE Stage"""

            offset = 5
            length = 1
            bit_mask = 32
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)

        class CH6EQATT:
            """Set attenuator in EQ"""

            offset = 3
            length = 2
            bit_mask = 24
            default = 3

            @staticmethod
            def validate(value):
                return value in range(4)

        class CH6CMLAMPLSEL:
            """Set the output amplitude"""

            offset = 0
            length = 3
            bit_mask = 7
            default = 7

            @staticmethod
            def validate(value):
                return value in range(8)

    class CH6UPLINK1:

        address = 0x10

        @staticmethod
        def __str__():
            return "CH6UPLINK1"

        @staticmethod
        def __int__():
            return 0x10

        class CH6CTLEHFSR:
            """Set the high Frequency CTLE peaking strength"""

            offset = 4
            length = 4
            bit_mask = 240
            default = 11

            @staticmethod
            def validate(value):
                return value in range(16)

        class CH6CTLEMFSR:
            """Set the middle Frequency CTLE peaking strength"""

            offset = 0
            length = 4
            bit_mask = 15
            default = 11

            @staticmethod
            def validate(value):
                return value in range(16)

    class CH6UPLINK2:

        address = 0x11

        @staticmethod
        def __str__():
            return "CH6UPLINK2"

        @staticmethod
        def __int__():
            return 0x11

        class CH6DIS:
            """Disable channel"""

            offset = 2
            length = 1
            bit_mask = 4
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)

        class CH6DISDFF:
            """Set Equalizer mode"""

            offset = 1
            length = 1
            bit_mask = 2
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)

        class CH6DISLPF:
            """disable DC offset cancellation"""

            offset = 0
            length = 1
            bit_mask = 1
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)

    class CH7UPLINK0:

        address = 0x12

        @staticmethod
        def __str__():
            return "CH7UPLINK0"

        @staticmethod
        def __int__():
            return 0x12

        class CH7DISEQLF:
            """disable the low Frequency CTLE Stage"""

            offset = 5
            length = 1
            bit_mask = 32
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)

        class CH7EQATT:
            """Set attenuator in EQ"""

            offset = 3
            length = 2
            bit_mask = 24
            default = 3

            @staticmethod
            def validate(value):
                return value in range(4)

        class CH7CMLAMPLSEL:
            """Set the output amplitude"""

            offset = 0
            length = 3
            bit_mask = 7
            default = 7

            @staticmethod
            def validate(value):
                return value in range(8)

    class CH7UPLINK1:

        address = 0x13

        @staticmethod
        def __str__():
            return "CH7UPLINK1"

        @staticmethod
        def __int__():
            return 0x13

        class CH7CTLEHFSR:
            """Set the high Frequency CTLE peaking strength"""

            offset = 4
            length = 4
            bit_mask = 240
            default = 11

            @staticmethod
            def validate(value):
                return value in range(16)

        class CH7CTLEMFSR:
            """Set the middle Frequency CTLE peaking strength"""

            offset = 0
            length = 4
            bit_mask = 15
            default = 11

            @staticmethod
            def validate(value):
                return value in range(16)

    class CH7UPLINK2:

        address = 0x14

        @staticmethod
        def __str__():
            return "CH7UPLINK2"

        @staticmethod
        def __int__():
            return 0x14

        class CH7DIS:
            """Disable channel"""

            offset = 2
            length = 1
            bit_mask = 4
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)

        class CH7DISDFF:
            """Set Equalizer mode"""

            offset = 1
            length = 1
            bit_mask = 2
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)

        class CH7DISLPF:
            """disable DC offset cancellation"""

            offset = 0
            length = 1
            bit_mask = 1
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)

    class PHASESHIFTER0:
        """Phase shifter DLL settings"""
        address = 0x15

        @staticmethod
        def __str__():
            return "PHASESHIFTER0"

        @staticmethod
        def __int__():
            return 0x15

        class DLLENABLE:
            """Enable DLL in phase Shifter"""

            offset = 1
            length = 1
            bit_mask = 2
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)

        class DLLCAPRESET:
            """Reset the control voltage in DLL"""

            offset = 0
            length = 1
            bit_mask = 1
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)

    class PHASESHIFTER1:
        """Phase shifter DLL settings"""
        address = 0x16

        @staticmethod
        def __str__():
            return "PHASESHIFTER1"

        @staticmethod
        def __int__():
            return 0x16

        class DLLFORCEDOWN:
            """Force down the charge pump output"""

            offset = 4
            length = 1
            bit_mask = 16
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)

        class DLLCHARGEPUMPCURRENT:
            """Set the Charge pump current"""

            offset = 0
            length = 4
            bit_mask = 15
            default = 15

            @staticmethod
            def validate(value):
                return value in range(16)

    class PHASESHIFTER2:
        """Phase shifter clock delay settings for channel 6 and 7"""
        address = 0x17

        @staticmethod
        def __str__():
            return "PHASESHIFTER2"

        @staticmethod
        def __int__():
            return 0x17

        class DLLCLOCKDELAYCH7:
            """Set clock delay of channel 7"""

            offset = 4
            length = 4
            bit_mask = 240
            default = 5

            @staticmethod
            def validate(value):
                return value in range(16)

        class DLLCLOCKDELAYCH6:
            """Set clock delay of channel 6"""

            offset = 0
            length = 4
            bit_mask = 15
            default = 5

            @staticmethod
            def validate(value):
                return value in range(16)

    class PHASESHIFTER3:
        """Phase shifter clock delay settings for channel 4 and 5"""
        address = 0x18

        @staticmethod
        def __str__():
            return "PHASESHIFTER3"

        @staticmethod
        def __int__():
            return 0x18

        class DLLCLOCKDELAYCH5:
            """Set clock delay of channel 5"""

            offset = 4
            length = 4
            bit_mask = 240
            default = 5

            @staticmethod
            def validate(value):
                return value in range(16)

        class DLLCLOCKDELAYCH4:
            """Set clock delay of channel 4"""

            offset = 0
            length = 4
            bit_mask = 15
            default = 5

            @staticmethod
            def validate(value):
                return value in range(16)

    class PHASESHIFTER4:
        """Phase shifter clock delay settings for channel 2 and 3"""
        address = 0x19

        @staticmethod
        def __str__():
            return "PHASESHIFTER4"

        @staticmethod
        def __int__():
            return 0x19

        class DLLCLOCKDELAYCH7:
            """Set clock delay of channel 3"""

            offset = 4
            length = 4
            bit_mask = 240
            default = 5

            @staticmethod
            def validate(value):
                return value in range(16)

        class DLLCLOCKDELAYCH6:
            """Set clock delay of channel 2"""

            offset = 0
            length = 4
            bit_mask = 15
            default = 5

            @staticmethod
            def validate(value):
                return value in range(16)

    class PHASESHIFTER5:
        """Phase shifter clock delay settings for test channel and 1"""
        address = 0x1a

        @staticmethod
        def __str__():
            return "PHASESHIFTER5"

        @staticmethod
        def __int__():
            return 0x1a

        class DLLCLOCKDELAYCH1:
            """Set clock delay of channel 1"""

            offset = 4
            length = 4
            bit_mask = 240
            default = 5

            @staticmethod
            def validate(value):
                return value in range(16)

        class DLLCLOCKDELAYCHTEST:
            """Set clock delay of test channel"""

            offset = 0
            length = 4
            bit_mask = 15
            default = 5

            @staticmethod
            def validate(value):
                return value in range(16)

    class LVDSRXTX:

        address = 0x1b

        @staticmethod
        def __str__():
            return "LVDSRXTX"

        @staticmethod
        def __int__():
            return 0x1b

        class RXEN:
            """Enable 1.28 GHz input clock receiver"""

            offset = 6
            length = 1
            bit_mask = 64
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)

        class RXSETCM:
            """Set the common mode voltage of clock receiver"""

            offset = 5
            length = 1
            bit_mask = 32
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)

        class RXENTERMINATION:
            """Enable the termination resistors of clock receiver"""

            offset = 4
            length = 1
            bit_mask = 16
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)

        class RXINVDATA:
            """Invert the clock receiver phase"""

            offset = 3
            length = 1
            bit_mask = 8
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)

        class RXEQ:
            """Set the passive EQ in clock receiver"""

            offset = 1
            length = 2
            bit_mask = 6
            default = 0

            @staticmethod
            def validate(value):
                return value in range(4)

        class DISTX:
            """Disable the test clock output"""

            offset = 0
            length = 1
            bit_mask = 1
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)

    class CH1DOWNLINK0:

        address = 0x1c

        @staticmethod
        def __str__():
            return "CH1DOWNLINK0"

        @staticmethod
        def __int__():
            return 0x1c

        class TX1DLATT:
            """Set the attenuator"""

            offset = 4
            length = 2
            bit_mask = 48
            default = 3

            @staticmethod
            def validate(value):
                return value in range(4)

        class TX1DISDLEMP:
            """Disable pre-emphasis"""

            offset = 3
            length = 1
            bit_mask = 8
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)

        class TX1DLSR:
            """Set the pre-emphasis strength"""

            offset = 0
            length = 3
            bit_mask = 7
            default = 5

            @staticmethod
            def validate(value):
                return value in range(8)

    class CH1DOWNLINK1:

        address = 0x1d

        @staticmethod
        def __str__():
            return "CH1DOWNLINK1"

        @staticmethod
        def __int__():
            return 0x1d

        class TX1DISDLBIAS:
            """Disable the channel"""

            offset = 1
            length = 1
            bit_mask = 2
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)

        class TX1DISDLLPFBIAS:
            """Disable the dc offset cancellation"""

            offset = 0
            length = 1
            bit_mask = 1
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)

    class CH2DOWNLINK0:

        address = 0x1e

        @staticmethod
        def __str__():
            return "CH2DOWNLINK0"

        @staticmethod
        def __int__():
            return 0x1e

        class TX2DLATT:
            """Set the attenuator"""

            offset = 4
            length = 2
            bit_mask = 48
            default = 3

            @staticmethod
            def validate(value):
                return value in range(4)

        class TX2DISDLEMP:
            """Disable pre-emphasis"""

            offset = 3
            length = 1
            bit_mask = 8
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)

        class TX2DLSR:
            """Set the pre-emphasis strength"""

            offset = 0
            length = 3
            bit_mask = 7
            default = 5

            @staticmethod
            def validate(value):
                return value in range(8)

    class CH2DOWNLINK1:

        address = 0x1f

        @staticmethod
        def __str__():
            return "CH2DOWNLINK1"

        @staticmethod
        def __int__():
            return 0x1f

        class TX2DISDLBIAS:
            """Disable the channel"""

            offset = 1
            length = 1
            bit_mask = 2
            default = 0

            @staticmethod
            def validate(value):
                return value in range(2)

        class TX2DISDLLPFBIAS:
            """Disable the dc offset cancellation"""

            offset = 0
            length = 1
            bit_mask = 1
            default = 1

            @staticmethod
            def validate(value):
                return value in range(2)

    @unique
    class Reg(IntEnum):
        CH1UPLINK0 = 0x00
        CH1UPLINK1 = 0x01
        CH1UPLINK2 = 0x02
        CH2UPLINK0 = 0x03
        CH2UPLINK1 = 0x04
        CH2UPLINK2 = 0x05
        CH3UPLINK0 = 0x06
        CH3UPLINK1 = 0x07
        CH3UPLINK2 = 0x08
        CH4UPLINK0 = 0x09
        CH4UPLINK1 = 0x0a
        CH4UPLINK2 = 0x0b
        CH5UPLINK0 = 0x0c
        CH5UPLINK1 = 0x0d
        CH5UPLINK2 = 0x0e
        CH6UPLINK0 = 0x0f
        CH6UPLINK1 = 0x10
        CH6UPLINK2 = 0x11
        CH7UPLINK0 = 0x12
        CH7UPLINK1 = 0x13
        CH7UPLINK2 = 0x14
        PHASESHIFTER0 = 0x15
        PHASESHIFTER1 = 0x16
        PHASESHIFTER2 = 0x17
        PHASESHIFTER3 = 0x18
        PHASESHIFTER4 = 0x19
        PHASESHIFTER5 = 0x1a
        LVDSRXTX = 0x1b
        CH1DOWNLINK0 = 0x1c
        CH1DOWNLINK1 = 0x1d
        CH2DOWNLINK0 = 0x1e
        CH2DOWNLINK1 = 0x1f
